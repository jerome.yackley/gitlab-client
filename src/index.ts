import * as rp from 'request-promise-native';
import * as args from 'yargs';
import {gitlabConfig} from './gitlab.config';

let story = args.argv['story'];
let mergeRequestId = args.argv['mr'];

async function run() {
  let mergeRequestBody = await rp({
    method: 'GET',
    uri: gitlabConfig.url + gitlabConfig.projectId + '/merge_requests/' + mergeRequestId + '/changes',
    headers: {'Private-Token': gitlabConfig.privateKey},
    resolveWithFullResponse: false
  });

  let mergeRequestChanges = JSON.parse(mergeRequestBody);

  return mergeRequestChanges.changes.map(change => {
      let output = '';
      if (change.new_file) {
        output += 'added:  ';
      } else if (change.deleted_file) {
        output += 'deleted:  ';
      } else {
        output += 'modified:  ';
      }

      output += change.new_path + ' |';
      output += ' XXXXX |';
      output += ' XXXXX |';
      output += ' ' + story;

      return output;
    });
}

run().then((res)=>{
  console.log(res.sort()
                 .join('\n'));
});
